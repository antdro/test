package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type candle struct {
	name    string
	minCost float32
	minTime string
	maxCost float32
	maxTime string
}

type trade struct {
	id       int
	ticker   string
	costBuy  float32
	costSell float32
}

// Pair id+ticker
type Pair struct {
	a, b interface{}
}

func main() {
	file, err := os.Create("results.csv")
	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}

	var candles []candle
	check := make(map[string]bool)

	fileCandles, err := os.Open("candles_5m.csv")
	if err != nil {
		os.Exit(1)
	}

	reader := bufio.NewReader(fileCandles)
	for {
		text, err := reader.ReadString('\n')
		text = strings.Trim(text, "\n")
		if err == io.EOF {
			break
		}
		arr := strings.Split(text, ",")

		candleName := arr[0]
		startTime := arr[1]
		tempmaxCost, err := strconv.ParseFloat(arr[3], 64)
		tempminCost, err := strconv.ParseFloat(arr[4], 64)

		maxCost := float32(tempmaxCost)
		minCost := float32(tempminCost)

		_, ok := check[candleName]
		if !ok {
			check[candleName] = true // добавление в словарь тикер свечи
			var app = candle{name: candleName, minCost: minCost, minTime: startTime, maxCost: maxCost, maxTime: startTime}
			candles = append(candles, app) // добавление свечи
			continue
		} else {
			var candleIndex int
			for i, s := range candles {
				if s.name == candleName {
					candleIndex = i
					break
				}
			}

			var temp *candle = &candles[candleIndex]

			if maxCost > temp.maxCost {
				temp.maxCost = maxCost
				temp.maxTime = startTime
			}
			if minCost < temp.minCost {
				temp.minCost = minCost
				temp.minTime = startTime
			}
		}

	}

	defer fileCandles.Close()

	fileTrades, err := os.Open("user_trades.csv")
	if err != nil {
		os.Exit(1)
	}

	defer file.Close()
	defer fileTrades.Close()

	var trades []trade
	checkTrade := make(map[Pair]bool)

	getIndex := make(map[Pair]int)

	reader = bufio.NewReader(fileTrades)
	for {
		text, err := reader.ReadString('\n')
		text = strings.Trim(text, "\n")
		if err == io.EOF {
			break
		}
		arr := strings.Split(text, ",")

		tempid, err := strconv.ParseInt(arr[0], 10, 32)
		id := int(tempid)
		ticker := arr[2]
		tempcostBuy, err := strconv.ParseFloat(arr[3], 32)
		tempcostSell, err := strconv.ParseFloat(arr[4], 32)

		costBuy := float32(tempcostBuy)
		costSell := float32(tempcostSell)

		t := Pair{id, ticker}

		_, ok := checkTrade[t]
		if !ok {
			checkTrade[t] = true // добавление в словарь пары id+ticker
			getIndex[t] = len(trades)
			var app = trade{id, ticker, costBuy, costSell}
			trades = append(trades, app) // добавление сделки
			continue
		} else {
			var candleIndex int = getIndex[t]
			for i, s := range trades {
				if s.id == id && s.ticker == ticker {
					candleIndex = i
					break
				}
			}

			var temp *trade = &trades[candleIndex]

			if costBuy != 0 {
				temp.costBuy = costBuy
			}
			if costSell != 0 {
				temp.costSell = costSell
			}
		}
	}

	for _, t := range trades {

		var index = -1

		for i, c := range candles {
			if t.ticker == c.name {
				index = i
				break
			}
		}

		var temp = candles[index]

		userRevenue := t.costSell - t.costBuy
		maxRevenue := (temp.maxCost) - (temp.minCost)
		diff := maxRevenue - userRevenue

		fmt.Fprintf(file, "%v,%s,%.2f,%.2f,%.2f,%s,%s\n", t.id, t.ticker, userRevenue, maxRevenue, diff, temp.maxTime, temp.minTime)
	}

}
